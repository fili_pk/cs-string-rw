pub struct ReadByteArr<'a> {
	index: usize,
	arr: &'a [u8],
}

impl<'a> ReadByteArr<'a> {
	pub fn new(arr: &'a [u8]) -> Self {
		Self { index: 0, arr }
	}
}

impl<'a> Iterator for ReadByteArr<'a> {
	type Item = u8;
	fn next(&mut self) -> Option<Self::Item> {
		if self.arr.len() <= self.index {
			return None;
		}

		let buf = self.arr[self.index];
		self.index += 1;
		Some(buf)
	}
}

impl<'a> std::io::Read for ReadByteArr<'a> {
	fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
		let mut readed_bytes = 0_usize;
		while readed_bytes < buf.len() {
			if let Some(byte) = self.next() {
				buf[readed_bytes] = byte;
				readed_bytes += 1;
			} else {
				break;
			}
		}

		Ok(readed_bytes)
	}
}
