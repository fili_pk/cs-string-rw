pub struct WriteByteVec<'a> {
	vec: &'a mut Vec<u8>,
}

impl<'a> WriteByteVec<'a> {
	pub fn new(vec: &'a mut Vec<u8>) -> Self {
		Self { vec }
	}
}

impl<'a> std::io::Write for WriteByteVec<'a> {
	fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
		self.vec.extend_from_slice(buf);
		Ok(buf.len())
	}

	fn flush(&mut self) -> std::io::Result<()> {
		Ok(())
	}
}
