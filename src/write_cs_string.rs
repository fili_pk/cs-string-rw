use crate::write_7_bit_encoded_i32::Write7BitEncodedI32Ext;
use std::io;

pub trait WriteCsStrExt {
	fn write_cs_string(&mut self, n: &str) -> io::Result<()>;
}

impl<T: io::Write> WriteCsStrExt for T {
	fn write_cs_string(&mut self, n: &str) -> io::Result<()> {
		self.write_7_bit_encoded_i32(n.len() as i32)?;
		self.write_all(n.as_bytes())
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::write_byte_vec::WriteByteVec;

	#[test]
	fn reading_cs_strings() {
		let mut vec = Vec::with_capacity(580);
		let mut writer = WriteByteVec::new(&mut vec);
		writer.write_cs_string("").unwrap();
		writer.write_cs_string("a").unwrap();
		writer.write_cs_string("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA").unwrap();
		writer.write_cs_string("📮").unwrap();
		writer
			.write_cs_string(
				"🏴‍☠️🏴‍☠️🏴‍☠️🏴‍☠️🏴‍☠️🏴‍☠️🏴‍☠️🏴‍☠️🏴‍☠️🏴‍☠️🏴‍☠️🏴‍☠️🏴‍☠️🏴‍☠️🏴‍☠️🏴‍☠️🏴‍☠️🏴‍☠️🏴‍☠️🏴‍☠️🏴‍☠️🏴‍☠️🏴‍☠️🏴‍☠️",
			)
			.unwrap();

		assert_eq!(
			vec,
			[
				0x00, 0x01, 0x61, 0x80, 0x02, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
				0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
				0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
				0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
				0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
				0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
				0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
				0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
				0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
				0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
				0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
				0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
				0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
				0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
				0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
				0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
				0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
				0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
				0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x04, 0xf0, 0x9f, 0x93, 0xae,
				0xb8, 0x02, 0xf0, 0x9f, 0x8f, 0xb4, 0xe2, 0x80, 0x8d, 0xe2, 0x98, 0xa0, 0xef, 0xb8,
				0x8f, 0xf0, 0x9f, 0x8f, 0xb4, 0xe2, 0x80, 0x8d, 0xe2, 0x98, 0xa0, 0xef, 0xb8, 0x8f,
				0xf0, 0x9f, 0x8f, 0xb4, 0xe2, 0x80, 0x8d, 0xe2, 0x98, 0xa0, 0xef, 0xb8, 0x8f, 0xf0,
				0x9f, 0x8f, 0xb4, 0xe2, 0x80, 0x8d, 0xe2, 0x98, 0xa0, 0xef, 0xb8, 0x8f, 0xf0, 0x9f,
				0x8f, 0xb4, 0xe2, 0x80, 0x8d, 0xe2, 0x98, 0xa0, 0xef, 0xb8, 0x8f, 0xf0, 0x9f, 0x8f,
				0xb4, 0xe2, 0x80, 0x8d, 0xe2, 0x98, 0xa0, 0xef, 0xb8, 0x8f, 0xf0, 0x9f, 0x8f, 0xb4,
				0xe2, 0x80, 0x8d, 0xe2, 0x98, 0xa0, 0xef, 0xb8, 0x8f, 0xf0, 0x9f, 0x8f, 0xb4, 0xe2,
				0x80, 0x8d, 0xe2, 0x98, 0xa0, 0xef, 0xb8, 0x8f, 0xf0, 0x9f, 0x8f, 0xb4, 0xe2, 0x80,
				0x8d, 0xe2, 0x98, 0xa0, 0xef, 0xb8, 0x8f, 0xf0, 0x9f, 0x8f, 0xb4, 0xe2, 0x80, 0x8d,
				0xe2, 0x98, 0xa0, 0xef, 0xb8, 0x8f, 0xf0, 0x9f, 0x8f, 0xb4, 0xe2, 0x80, 0x8d, 0xe2,
				0x98, 0xa0, 0xef, 0xb8, 0x8f, 0xf0, 0x9f, 0x8f, 0xb4, 0xe2, 0x80, 0x8d, 0xe2, 0x98,
				0xa0, 0xef, 0xb8, 0x8f, 0xf0, 0x9f, 0x8f, 0xb4, 0xe2, 0x80, 0x8d, 0xe2, 0x98, 0xa0,
				0xef, 0xb8, 0x8f, 0xf0, 0x9f, 0x8f, 0xb4, 0xe2, 0x80, 0x8d, 0xe2, 0x98, 0xa0, 0xef,
				0xb8, 0x8f, 0xf0, 0x9f, 0x8f, 0xb4, 0xe2, 0x80, 0x8d, 0xe2, 0x98, 0xa0, 0xef, 0xb8,
				0x8f, 0xf0, 0x9f, 0x8f, 0xb4, 0xe2, 0x80, 0x8d, 0xe2, 0x98, 0xa0, 0xef, 0xb8, 0x8f,
				0xf0, 0x9f, 0x8f, 0xb4, 0xe2, 0x80, 0x8d, 0xe2, 0x98, 0xa0, 0xef, 0xb8, 0x8f, 0xf0,
				0x9f, 0x8f, 0xb4, 0xe2, 0x80, 0x8d, 0xe2, 0x98, 0xa0, 0xef, 0xb8, 0x8f, 0xf0, 0x9f,
				0x8f, 0xb4, 0xe2, 0x80, 0x8d, 0xe2, 0x98, 0xa0, 0xef, 0xb8, 0x8f, 0xf0, 0x9f, 0x8f,
				0xb4, 0xe2, 0x80, 0x8d, 0xe2, 0x98, 0xa0, 0xef, 0xb8, 0x8f, 0xf0, 0x9f, 0x8f, 0xb4,
				0xe2, 0x80, 0x8d, 0xe2, 0x98, 0xa0, 0xef, 0xb8, 0x8f, 0xf0, 0x9f, 0x8f, 0xb4, 0xe2,
				0x80, 0x8d, 0xe2, 0x98, 0xa0, 0xef, 0xb8, 0x8f, 0xf0, 0x9f, 0x8f, 0xb4, 0xe2, 0x80,
				0x8d, 0xe2, 0x98, 0xa0, 0xef, 0xb8, 0x8f, 0xf0, 0x9f, 0x8f, 0xb4, 0xe2, 0x80, 0x8d,
				0xe2, 0x98, 0xa0, 0xef, 0xb8, 0x8f,
			]
		);
	}
}
